Title: Introducción
Date: 2023-07-21 10:20
Category: 0. Introducción
Ordinal: 001


<h1> Introducción </h1>
La capacidad de poder detectar precisamente la dirección de mirada de los individuos se ha vuelto un punto central de interés debido a sus diversas aplicaciones en las áreas de la medicina, marketing, interacciones hombre-máquina, etc. Un posible acercamiento a este problema es utilizando ''pupil center-corneal reflection'' (PCCR), la cual se encarga de estimar la dirección de la mirada en imágenes utilizando LEDs  que se reflejan en la córnea de los ojos y podemos detectar como brillos o ''glint''. Con la posición de estos ''glint'' y la pupila podemos estimar la dirección de la mirada del individuo.

En este proyecto, nuestro objetivo principal es desarrollar un método para la detección de estos ''glint'', que es uno de los pasos clave en el proceso de determinar la dirección de la mirada utilizando PCCR. Para llevar a cabo esta tarea, contaremos con una base de datos que incluye imágenes de individuos con un ojo recortado, donde los LEDs de la cámara están dispuestos horizontalmente.

