Title: Métodos
Date: 2023-07-03
Category: 1. Métodos
Ordinal: 002

<h1>Métodos</h1>
<h2> Detección de Glint </h2>

El sistema que se va a implementar debe ser capaz de recibir una imágen, detectar los posibles glint y devolver la posición de estos. En la Figura 1 se presenta un conjunto de imágenes de la base de datos, en donde se resaltan los glints con un eclipse rojo, se puede apreciar que los mismos presentan un nivel de gris elevado, lo cual va a ser útil para su detección. Además se puede apreciar las pupilas del individuo, las mismas nos van a ser útil para poder definir el área de búsqueda de los glints.


<img alt="Figura 1: Imagenes usadas para la deteccion" src="{attach}/images/refDB.png" title="Figura 1: Imagenes usadas para la deteccion."/>

El procedimiento para la implementación del detector de glints es realizado diferentes iteraciones del mismo algoritmos, en donde en cada versión se busca resolver problemas con la detección de estos y la disminución del error, por lo cual cada vez se le agrega una capa mayor de complejidad. 

Cabe destacar que la base de datos que se utiliza presenta 540 imágenes, además tiene un archivo ‘’.csv’’  con las las posiciones de la pupila y los glints marcadas por un humano, además de una columna "mask"   que vale 1 cuando los glints aparecen correctamente en el ojo, y vale 0 cuando por algún tema de  adquisición no se ve uno o los dos glints. Para verificar el correcto funcionamiento utilizamos las imágenes con los glints que aparecen correctamente (507 imágenes).

Debido a que los glints presentan una distribución de sus valores de gris en forma de campana de Gauss, tomando su valor máximo en el centro, nuestra metodología de búsqueda es de buscar las regiones donde los niveles de gris sean elevados y de los contornos que presentan formas semejantes a circunferencias, encontrar su centro.


<h3> Versión 0 </h3>
Como primer acercamiento utilizamos umbrales estáticos (threshold=85) para poder clasificar la imagen según su nivel de gris, debido a que buscamos detectar los puntos que se caracterizan por su elevado valor de gris, nos vamos a quedar con la zona que se encuentra por encima del umbral. De estas regiones, encontramos sus contornos, de los cuales solo nos vamos a quedar con los que presentan un áreas pequeñas y que no se extiendan en la imágen. De estos contornos vamos a calcular sus centros con una función auxiliar.

Finalmente, es necesario elegir el par de puntos que nos vamos a quedar. Como la función que utilizamos para encontrar los contornos los ordena jerárquicamente por su localización, por lo cual nos vamos a  quedar con los dos primeros contornos.

Este método presenta diversos problemas, el más significativo es la selección directa de glints que se realiza, esto se va a abordar en la siguiente versión.


<h3> Versión 1 </h3>

Sabemos que los glints se encuentran a la misma altura debido al posicionamiento de los leds en la cámara y a una distancia horizontal entre sí que depende de la distancia del individuo a la cámara, lo cual al no variar en gran medida, genera un rango de distancia entre estos muy acotado. 

Teniendo esto en consideración, en esta versión se utiliza una función auxiliar la cual se encarga de seleccionar el par de puntos en función de esta posición relativa entre sí. Por lo cual se recorre todos los centros de los contornos y los agrupa por proximidad en el eje horizontal, una vez agrupados estos se busca aplicarles una función H la cual se minimiza si el par de puntos se encuentra a un valor D de píxeles.


<h3> Versión 2 </h3>

Al implementar las versiones anteriores se observó que un factor muy importante para la correcta detección de glints es el valor que se utiliza como umbral, en donde el valor de brillo de los glints es relativo al brillo global y un umbral estático puede funcionar bien en gran parte de los casos, pero no es todos.
Debido a esto se busca implementar un umbral dinámico el cual depende del nivel global de iluminación de la imagen. Para la elección del umbral se obtiene el histograma de la imagen y se calcula el máximo de este, en donde el valor donde se llega al máximo es del valor de gris que más veces se repite en la imágen y que nos sirve como una estimación de la iluminación en la imagen. 

<h3> Versión 3 </h3>

Para la detección de la mirada es necesario detectar la posición de la pupila, la cual debido a sus características, es más fácil de detectar correctamente. Debido a que para el PCCR necesitamos a ambos, nos puede ser de ayuda tener la ubicación de la pupila para seleccionar los centros de los contornos que se encuentran próximos a la pupila.

Cabe destacar que en esta versión no se hace un filtrado pasa bajos de la imagen umbralizada, esto es debido que el ruido de alta frecuencia no se encuentra entorno al ojo y al no filtrar perdemos menos información con respecto a los glintz (que se caracterizan por presentar gran parte de su información en las altas frecuencias debido a su cambios bruscos).




