Title: Conclusiones
Date: 2023-07-03
Category: 3. Conclusiones
Ordinal: 004


Se llevó a cabo un estudio exhaustivo del problema de detección de la mirada, y se logró implementar una etapa clave del método ''pupil center-corneal reflection'' (PCCR), que se enfoca en la detección de los reflejos o ''glints''. Además, se desarrollaron varias revisiones del detector de ''glints'', cada una con enfoques diferentes, y se evaluó su rendimiento comparando el error de detección en una base de datos.

Como resultado de las pruebas y análisis realizados, se llegó a la conclusión de que el detector de pupilas ofrece un rendimiento superior y una mayor fiabilidad. Esto permitió utilizarlo como guía para mejorar la detección de los ''glints''. También se observó que para lograr una versión más completa y precisa del detector, sería necesario implementar umbrales dinámicos alternativos que permitan diferenciar ambos tipos de ''glints'' de manera más efectiva.

Estos hallazgos son valiosos, ya que proporcionan una base sólida para continuar el desarrollo de la detección de la mirada utilizando la técnica PCCR. Las mejoras en la detección de ''glints'' tienen el potencial de mejorar significativamente la precisión y la robustez del sistema en general.