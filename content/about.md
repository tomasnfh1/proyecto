Title: Acerca de ...
Date: 2023-07-03
Category: 4. Acerca de ...
Ordinal: 005


El siguiente texto corresponde a la presentación de un trabajo realizado como proyecto de fin de curso de Tratamiento de Imágenes por Computadora:

Integrantes del grupo:

    Tomas Ferraz: Estudiante de Ingeniería Biológica.
    Contacto: tomasnfh1@gmail.com

Tutor:

    Alvaro Gomez


Bibliografía:

Wang, J., Zhang, G., & Shi, J. (2015). Pupil and Glint Detection Using Wearable Camera Sensor and Near-Infrared LED Array. Sensors (Basel, Switzerland), 15(12), 30126–30141. https://doi.org/10.3390/s151229792

Tonsen, M., Zhang, X., Sugano, Y., Bulling, A. (2016). Labelled pupils in the wild: a dataset for studying pupil detection in unconstrained environments. 139-142. doi = 10.1145/2857491.2857520

Otsu, N. (1979). A threshold selection method from gray-level histograms. IEEE transactions on systems, man, and cybernetics, 9(1), 62-66.

Yeon_ . (2016, April 30). pupil detection in OpenCV & Python
. https://stackoverflow.com/questions/36956278/pupil-detection-in-opencv-python
