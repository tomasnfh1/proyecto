Title: Resultados y Discuciones
Date: 2023-07-03
Category: 2. Resultados y Discuciones
Ordinal: 003


<h1>Resultados y conclusiones</h1>

<h2> Errores de detección </h2>

Para comprobar el funcionamiento del sistema se recorrió la base de datos con todas las versiones del sistema y  se calculó el error en la posición de los glints. Este error se presenta en gráficas que nos devuelve el porcentaje de la base de datos que tiene un error menor a un valor de píxeles.

En la Figura 2 se representa el error para cada glint por separado, en donde se busca representar que la versión base al no seleccionar los puntos, para cada glintz tenemos un error diferente y esto se debe a cómo se ordenan los contornos. Para las demás versiones esto no pasa, es decir que el error se distribuye de forma idéntica para ambos glints.

<img alt="Figura 2" src="{attach}/images/LRglint.png" title="Figura 2"/>

En la Figura 3 tenemos el error de ambas glints, en donde se puede observar el rendimiento de todos las versiones. Nos interesa destacar que para un error menor o igual a 20 pixeles, la última versión casi logra obtener un 60%, lo cual significa que la mayor parte de las imágenes de la base de datos tienen un error entre ambos glints menor a 20 píxeles.


En relación a las demás versiones se observa que las versiones 1 y 2 se distribuyen muy similarmente, y esto es debido a que los umbrales dinámicos mejoran muy levemente el error de detección. Por lo cual podría ser interesante utilizar otro método para calcular umbrales dinámicos (como por ejemplo  lo pueden ser los umbrales Otsu).


<img alt="Figura 3" src="{attach}/images/bothglintE.png" title="Figura 3"/>

Se puede apreciar en la Figura 4 que el error de ambos glintz para el 40% de la base de datos es menor a 5 pixeles, lo cual es lo buscado debido a que el error de cada píxel nos va a generar errores de la escala de centímetros a la hora de la estimación de la dirección de la mirada.


<img alt="Figura 4" src="{attach}/images/bothglintEZOOM.png" title="Figura 4"/>

En la Figura 5 se presenta el error correspondiente a las pupilas, en donde se puede apreciar que el error en comparación a la detección es mucho menor y lo cual nos avala que la información que utilizamos para la versión 3 es de calidad.


<img alt="Figura 5" src="{attach}/images/pupilE.png" title="Figura 5"/>






<h2> Casos de error </h2>

<h3> Elección de umbrales: </h3>


En las Figuras 6 se puede observar que la pupila es detectada correctamente, pero no se pueden encontrar ambos glints debido a un umbral muy bajo Figura 7. Esto provoca que el detector tome un par de puntos que pertenecen al ruido como glints debido a que cumplen las condiciones de distancia Figura 8.

<img alt="Figura 6" src="{attach}/images/45pupil.png" title="Figura 6"/>
<img alt="Figura 7" src="{attach}/images/45points.png" title="Figura 7"/>
<img alt="Figura 8" src="{attach}/images/45out.png" title="Figura 8"/>

Como primera medida para mitigar este problema es la mejor utilización de umbrales dinámicos. Otra alternativa es aumentar el rol en la toma de decisión del detector de pupilas,  esto  debido al buen funcionamiento del mismo, por lo cual  podemos llamar recursivamente la función con umbrales cada vez menores hasta obtener ambos glints en un entorno próximo al centro de la pupila.

<h3> Detector de pupilas: </h3>

En menor medida al problema anterior, tenemos casos en donde el detector de pupilas no es capaz de encontrar la pupila correctamente Figura 9, lo cual genera que la detección de glints sea defectuosa Figura 11. Cabe destacar que si se encontraron los puntos correspondientes a los glints Figura 10, pero se selecciona mal el par de puntos.

<img alt="Figura 9" src="{attach}/images/125pupil.png" title="Figura 9"/>
<img alt="Figura 10" src="{attach}/images/125points.png" title="Figura 10"/>
<img alt="Figura 11" src="{attach}/images/125out.png" title="Figura 11"/>

Una posible solución es complejizar el detector de pupilas, esto se podría hacer con una detección de la geometría ocular.




